# Generated by Django 2.1.5 on 2019-02-05 19:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('djangocms_page_meta', '0010_auto_20180108_2316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pagemeta',
            name='extended_object',
            field=models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='cms.Page'),
        ),
        migrations.AlterField(
            model_name='pagemeta',
            name='public_extension',
            field=models.OneToOneField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='draft_extension', to='djangocms_page_meta.PageMeta'),
        ),
        migrations.AlterField(
            model_name='titlemeta',
            name='extended_object',
            field=models.OneToOneField(editable=False, on_delete=django.db.models.deletion.CASCADE, to='cms.Title'),
        ),
        migrations.AlterField(
            model_name='titlemeta',
            name='public_extension',
            field=models.OneToOneField(editable=False, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='draft_extension', to='djangocms_page_meta.TitleMeta'),
        ),
    ]
